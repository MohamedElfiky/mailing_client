# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::UsersController, type: :controller do
  context 'with Admin' do
    login_admin

    let!(:user) { create :user }

    describe 'GET #index' do
      it 'returns http success' do
        get :index
        expect(response).to have_http_status(:success)
        expect(assigns(:users).map(&:id)).to eq(User.all.map(&:id))
      end
    end

    describe 'GET #new' do
      it 'returns http success' do
        get :new
        expect(response).to have_http_status(:success)
        expect(assigns(:user)).to be_a_new(User)
      end
    end

    describe 'POST #create' do
      it 'creates a new user with valid params' do
        expect do
          post :create, params: { user: attributes_for(:user) }
        end.to change(User, :count).by(1)
        expect(response).to redirect_to(:admin_users)
      end

      it 'create a new user with invalid params' do
        post :create, params: { id: user.id, user: { name: '' } }
        expect(response).to render_template(:new)
        expect(flash[:notice]).to eq(nil)
      end
    end

    describe 'PUT #update' do
      it 'update a new user with valid params without updating password' do
        put :update, params: { id: user.id, user: { name: 'mohamed' } }
        user.reload
        expect(response).to redirect_to(admin_user_url(user))
        expect(user.name).to eq('mohamed')
        expect(flash[:notice]).to eq('User was successfully updated.')
      end

      it 'update a new user with valid params and password' do
        params = attributes_for(:user)
        put :update, params: { id: user.id, user: params }
        user.reload
        expect(response).to redirect_to(admin_user_url(user))
        expect(user.name).to eq(params[:name])
        expect(flash[:notice]).to eq('User was successfully updated.')
      end

      it 'update a new user with invalid params' do
        put :update, params: { id: user.id, user: { name: '' } }
        expect(response).to render_template(:edit)
        expect(flash[:notice]).to eq(nil)
      end
    end

    describe 'GET #edit' do
      it 'returns http success' do
        get :edit, params: { id: admin.id }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #show' do
      it 'returns http success' do
        get :show, params: { id: admin.id }
        expect(response).to have_http_status(:success)
        expect(assigns(:user)).to eq(admin)
      end
    end

    describe 'GET #destroy' do
      it 'returns http redirect to home' do
        expect do
          delete :destroy, params: { id: user.id }
        end.to change(User, :count).by(-1)
        expect(response).to redirect_to(:admin_users)
      end
    end
  end

  context 'with Normal User' do
    login_user

    describe 'GET #index' do
      it 'returns http redirect to home' do
        get :index
        expect(response).to redirect_to(:root)
        expect(flash[:alert]).to eq('You are not authorized to perform this action.')
      end
    end

    describe 'GET #new' do
      it 'returns http redirect to home' do
        get :new
        expect(response).to redirect_to(:root)
        expect(flash[:alert]).to eq('You are not authorized to perform this action.')
      end
    end

    describe 'GET #edit' do
      it 'returns http redirect to home' do
        get :edit, params: { id: user.id }
        expect(response).to redirect_to(:root)
        expect(flash[:alert]).to eq('You are not authorized to perform this action.')
      end
    end

    describe 'GET #show' do
      it 'returns http redirect to home' do
        get :show, params: { id: user.id }
        expect(response).to redirect_to(:root)
        expect(flash[:alert]).to eq('You are not authorized to perform this action.')
      end
    end

    describe 'GET #delete' do
      it 'returns http redirect to home' do
        delete :destroy, params: { id: user.id }
        expect(response).to redirect_to(:root)
        expect(flash[:alert]).to eq('You are not authorized to perform this action.')
      end
    end
  end
end
