# frozen_string_literal: true

FactoryGirl.define do
  factory :user, aliases: %i(sender recipient) do
    sequence(:name) { |n| "jdoe#{n}" }
    sequence(:email) { |n| "john.doe+#{Time.current.to_i}#{n}@example.com" }
    password '12345678'
    password_confirmation '12345678'
    role :user

    trait :admin do
      role :admin
    end
  end
end
