# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def custom_mail(email)
    @email = email
    mail from: email.sender.info, to: email.recipients.map(&:email).join(','), subject: email.subject
  end
end
