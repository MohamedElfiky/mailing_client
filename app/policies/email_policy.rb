# frozen_string_literal: true

class EmailPolicy < ApplicationPolicy
  def index?
    true
  end

  def sent?
    true
  end

  def show?
    true
  end

  def create?
    true
  end
end
