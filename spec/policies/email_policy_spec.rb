# frozen_string_literal: true

require 'rails_helper'

describe EmailPolicy do
  subject { described_class }

  permissions :index?, :sent?, :show?, :new?, :create? do
    it 'grants access for all users on mail' do
      expect(subject).to permit(User.new, Email.new)
    end
  end
end
