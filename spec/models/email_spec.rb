# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Email, type: :model do
  it { should belong_to :sender }
  it { should have_and_belong_to_many :recipients }
  it { should validate_presence_of(:subject) }
  it { should validate_presence_of(:body) }
  it { should validate_presence_of(:recipients) }
  it { should validate_presence_of(:sender) }
end
