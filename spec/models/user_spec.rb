# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_and_belong_to_many :recived_emails }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_presence_of(:password_confirmation) }

  context 'User#info' do
    let(:user) { build(:user) }
    it 'equals name and mail' do
      expect(user.info).to eq("#{user.name} <#{user.email}>")
    end
  end
end
