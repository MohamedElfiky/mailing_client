# frozen_string_literal: true

require 'rails_helper'

describe ApplicationPolicy do
  subject { described_class }

  permissions :update?, :edit?, :index?, :show?, :new?, :create?, :destroy? do
    it 'denies access for all actions by default' do
      expect(subject).not_to permit(nil, nil)
    end
  end
end
