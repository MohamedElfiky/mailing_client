# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'no-replay@mailingclient.com'
  layout 'mailer'
end
