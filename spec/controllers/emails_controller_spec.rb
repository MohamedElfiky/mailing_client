# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EmailsController, type: :controller do
  login_user
  let!(:email) { create(:email) }

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'assigns emails to only user recived_emails' do
      user_email = create(:email)
      user_email.recipients << user
      get :index
      expect(assigns(:emails)).to eq([user_email])
    end
  end

  describe 'GET #sent' do
    it 'returns http success' do
      get :sent
      expect(response).to have_http_status(:success)
    end

    it 'assigns emails to only user sent_emails' do
      email = create(:email, sender: user)
      get :sent
      expect(assigns(:emails)).to eq([email])
    end
  end

  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
      expect(assigns(:email)).to be_a_new(Email)
    end
  end

  describe 'GET #show' do
    it 'returns http success' do
      get :show, params: { id: email.id }
      expect(response).to have_http_status(:success)
    end

    it 'returns email with id' do
      get :show, params: { id: email.id }
      expect(assigns(:email)).to eq(email)
    end
  end

  describe 'POST #create' do
    it 'returns http success' do
      expect do
        params = attributes_for(:email).merge(recipients: [user.id])
        post :create, params: { email: params }
      end.to change(Email, :count).by(1)

      expect(response).to redirect_to(:emails)
      expect(flash[:notice]).to eq('Email sent successfully.')
    end

    it 'create recipients list with user create' do
      params = attributes_for(:email).merge(recipients: [user.id])
      post :create, params: { email: params }
      expect(assigns(:email).recipients).to include(user)
    end

    it 'create recipients list with user create' do
      ActionMailer::Base.deliveries.clear
      params = attributes_for(:email).merge(recipients: [user.id])
      post :create, params: { email: params }
      expect(ActionMailer::Base.deliveries.count).to eq(1)
    end

    it "can't create mail with invalid params" do
      expect do
        post :create, params: { email: { subject: '' } }
      end.to change(Email, :count).by(0)
      expect(response).to render_template(:new)
    end
  end
end
