# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EmailsHelper do
  context '#system_users' do
    it 'equals name and mail' do
      user = create(:user)
      expect(helper.system_users).to include([user.info, user.id])
    end
  end

  context '#recipients' do
    let!(:email) { create(:email) }
    let(:user) { email.recipients.first }
    
    it 'format recipients with only names' do
      expect(helper.recipients(email))
        .to eq("<div class=\"recipients-list\"><span title=\"#{user.email}\" class=\"label label-default\">#{user.name}</span></div>")
    end

    it 'format recipients info' do
      expect(helper.recipients(email, true))
        .to eq("<div class=\"recipients-list\"><span>#{CGI::escapeHTML(user.info)}</span></div>")
    end
  end
end
