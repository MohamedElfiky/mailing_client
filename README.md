# Mailing Client
http://mailclient1.herokuapp.com

## Setup
Change database.yml configration to your config, then run `./bin/setup` after cloning this repo. It will install all dependencies, create database and run seeds.

## Environment variables
to send emails using mailgun you must have `MAILGUN_API_KEY` and `MAILGUN_DOMAIN` on your envirnoment variables containing mailgun credentials.

## Server
Just `rails s` to run application server.

## Ruby version
ruby 2.3.3


## Testing
You are free to use `rake`, `rake spec` or even `rspec` command to run test suite against whole application.
