# frozen_string_literal: true

module EmailsHelper
  def system_users
    User.all.map { |u| [u.info, u.id] }
  end

  def recipients(email, info = false)
    content_tag :div, class: 'recipients-list' do
      email.recipients.collect do |recipient|
        if info
          concat content_tag(:span, recipient.info)
        else
          concat content_tag(:span, recipient.name, title: recipient.email, class: 'label label-default')
        end
      end
    end
  end
end
