# frozen_string_literal: true

class EmailsController < ApplicationController
  before_action :authenticate_user!

  def index
    authorize :email
    @emails = current_user.recived_emails.preload(:sender).latest(params[:page])
  end

  def sent
    authorize :email
    @emails = current_user.sent_emails.preload(:recipients).latest(params[:page])
  end

  def new
    @email = Email.new
    authorize @email
  end

  def show
    @email = Email.preload(:sender, :recipients).find(params[:id])
    authorize @email
  end

  def create
    @email = Email.new(email_params)
    authorize @email
    @email.recipients = User.where(id: params[:email][:recipients])
    return redirect_to(emails_url, notice: 'Email sent successfully.') if @email.save
    render :new
  end

  private

  def email_params
    params.require(:email).permit(:subject, :body).merge(sender: current_user)
  end
end
