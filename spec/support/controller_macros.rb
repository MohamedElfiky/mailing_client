# frozen_string_literal: true

module ControllerMacros
  def login_admin
    let(:admin) { create(:user, :admin) }
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:admin]
      sign_in admin
    end
  end

  def login_user
    let(:user) { create(:user) }
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      sign_in user
    end
  end
end
