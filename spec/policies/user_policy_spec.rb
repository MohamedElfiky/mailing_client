# frozen_string_literal: true

require 'rails_helper'

describe UserPolicy do
  subject { described_class }

  permissions :update?, :edit?, :index?, :show?, :new?, :create?, :destroy? do
    it 'grants access if user is admin' do
      expect(subject).not_to permit(User.new(role: :user), User.new)
    end

    it 'denies access if user not admin' do
      expect(subject).to permit(User.new(role: :admin), User.new)
    end
  end
end
