# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery.fn.extend format_datetime: ->
  elDate = new moment($(this).text())
  currentDate = new moment
  if currentDate.isSame(elDate, 'day')
    $(this).text elDate.format('h:mm a')
  else if currentDate.isSame(elDate, 'year')
    $(this).text elDate.format('MMM DD')
  else
    $(this).text elDate.format('MM/DD/YYYY')
  return
