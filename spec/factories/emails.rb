# frozen_string_literal: true

FactoryGirl.define do
  factory :email do
    sequence(:subject) { |n| "subject #{n}" }
    sequence(:body) { |n| "email body #{n}" }
    sender

    after(:build) { |email| email.recipients.push(build(:user)) }
  end
end
