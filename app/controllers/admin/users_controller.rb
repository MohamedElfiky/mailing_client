# frozen_string_literal: true

module Admin
  class UsersController < AdminController
    before_action :set_user, only: %i(edit update destroy show)

    def index
      authorize :user
      @users = User.all.page(params[:page] || 0)
    end

    def new
      @user = User.new
      authorize @user
    end

    def create
      @user = User.new(user_params)
      authorize @user

      return redirect_to(admin_users_url, notice: 'User was successfully created.') if @user.save
      render :new
    end

    def show; end

    def edit; end

    def update
      return redirect_to(admin_user_url(@user), notice: 'User was successfully updated.') if @user.update(user_params)
      render :edit
    end

    def destroy
      @user.destroy
      redirect_to admin_users_url, notice: 'User was successfully destroyed.'
    end

    private

    # get user params from request params and ignore password and password_confirmation if blank
    def user_params
      params
        .require(:user)
        .permit(:name, :email, :password, :password_confirmation, :role)
        .delete_if { |k, v| %w(password password_confirmation).include?(k) && v.blank? }
    end

    def set_user
      @user = User.find(params[:id])
      authorize @user
    end
  end
end
