class CreateEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :emails do |t|
      t.string :subject
      t.text :body
      t.references :sender, references: :users

      t.timestamps
    end
  end
end
