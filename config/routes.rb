# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :admin do
    resources :users
  end

  resources :emails, only: %i(index new create show) do
    get :sent, on: :collection
  end
  devise_for :users
  root to: 'pages#index'
end
