# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
  enum role: %i(user admin)
  has_and_belongs_to_many :recived_emails, class_name: 'Email'
  has_many :sent_emails, class_name: 'Email', foreign_key: :sender_id
  validates :name, :email, presence: true
  validates :password, :password_confirmation, presence: true, on: :create

  def info
    "#{name} <#{email}>"
  end
end
