
# frozen_string_literal: true

User.where(email: 'admin@mailingclient.com').first_or_create(name: 'admin',
                                                             role: User.roles[:admin],
                                                             password_confirmation: 'ChangeMe!',
                                                             password: 'ChangeMe!')
