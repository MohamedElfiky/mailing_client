# frozen_string_literal: true

class Email < ApplicationRecord
  belongs_to :sender, class_name: 'User'
  has_and_belongs_to_many :recipients, class_name: 'User'

  validates :subject, :body, :recipients, :sender, presence: true

  after_save :send_mail
  scope :latest, ->(page) { order(created_at: :desc).page(page || 1) }

  def send_mail
    UserMailer.custom_mail(self).deliver_now
  end
end
